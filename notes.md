## Design details
The core app should be wrapped in a app-wrap component, with the navigation and a router-view.

### Segments
Like a sheet for an excel file, you can create a segment for a part of the database that are related.
Possibly in bigger databases this might be useful.
