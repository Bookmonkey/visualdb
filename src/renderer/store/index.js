import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    connections: [],
  },
  getters: {
    getConnections: state => {
      return state.connections;
    }
  },
  mutations: {
    addConnection: (state, connection) => {
      console.log(connection);
    }
  },
  actions: {
    addConnection: ({ commit }, connection) => {
      commit('addConnection', connection);
    }
  }
});