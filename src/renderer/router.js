import Vue from "vue";

import Router from "vue-router";

import Home from "./views/Homepage";
import Database from './views/Database.vue';
import Settings from './views/Settings';

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: '/database',
      name: "Database",
      component: Database
    },
    {
      path: '/settings',
      name: "Settings",
      component: Settings
    }
  ]
});

export default router;