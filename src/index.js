import { app, BrowserWindow, ipcMain } from 'electron';
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer';
import { enableLiveReload } from 'electron-compile';

import os from "os";

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

const isDevMode = process.execPath.match(/[\\/]electron/);


import { CONFIG } from "../config";

const platform = os.platform();

// add to the process.env path
if(platform === "win32"){ 
  process.env['PATH'] += `;${CONFIG.psqlPath}`;
}


console.log(process.env['PATH']);



if (isDevMode) enableLiveReload();

const createWindow = async () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
  },
);

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/renderer/index.html`);

  // Open the DevTools.
  if (isDevMode) {
    await installExtension(VUEJS_DEVTOOLS);
    mainWindow.webContents.openDevTools();
  }

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

let currentConnectionString = "";

import { exec } from "child_process";

ipcMain.on('DB_CONNECT', async (event, arg) => {
  let tables = [], constraints = [];

  let connection = arg.connection;

  currentConnectionString = `postgres://${connection.username}:${connection.password}@${connection.server}/${connection.table}`;

  console.log(currentConnectionString);
  try {
    tables = await getTableNames();
    tables = tables.map(ele => ele.trim());

    tables = await Promise.all(tables.map(async ele => {
      return {
        name: ele,
        tableDefintion: await getTableColumns(ele),
      }
    }));

    constraints = await getConstraints();

  } catch (error) {
    console.error(error);
  }
  finally{
    event.sender.send("DB_CONNECTED", {
      tables,
      constraints
    });
  }

  
});

function createQuery(keyName, tableName) {
  return QUERIES[keyName](tableName);
}
function getTableNames() {
  let query = `psql -t -f ./scripts/pg/getTables.sql ${currentConnectionString}`;
  return new Promise((resolve, reject) => {

    exec(query, (err, stdout, stderr) => {
      if (err) reject(err);
      if (stderr) reject(stderr);

      stdout = stdout.split('\n');
      stdout = stdout.filter(ele => ele.length > 0);

      resolve(stdout);
    }); 
  });
}


function getTableColumns(tableName) {

  let query = `echo "${createQuery('getTableDefintion', tableName)}" | ${createQuery('connectionString')}`;
  // let query = `echo "${}" | `;

  return new Promise((resolve, reject) => {
    exec(query, (err, stdout, stderr) => {
      if (err) reject(err);
      if (stderr) reject(stderr);
      // resolve(stdout);

      let result = [];

      let columns = stdout.split("\n");

      columns.map(ele => {
        if(ele.length > 0) {
          let split = ele.split("|");
          result.push({
            name: split[0].trim(),
            datatype: split[1].trim()
          });
        }
      });

      resolve(result);
    }); 
  })
}

function getConstraints() {
  let query = `echo "${createQuery('getConstraints')}" | ${createQuery('connectionString')}`;
  return new Promise((resolve, reject) => {
    exec(query, (err, stdout, stderr) => {
      if (err) reject(err);
      if (stderr) reject(stderr);
  
      stdout = stdout.split('\n');
      stdout = stdout.filter(ele => ele.length > 0);
      stdout = stdout.map(ele => {
        let split = ele.split("|");
        return {
          constraint_name: split[0].trim(),
          constraint_type: split[1].trim(),
          table_name: split[2].trim(),
          column_name: split[3].trim(),
          is_deferrable: split[4].trim(),
          initially_deferred: split[5].trim(),
          match_type: split[6].trim(),
          on_update: split[7].trim(),
          on_delete: split[8].trim(),
          references_table: split[9].trim(),
          references_field: split[10].trim(),
        }
  
      });
  
      resolve(stdout);
    });
  });
}



const QUERIES = {
  getTableDefintion: (table) => {
    return `select column_name, data_type from information_schema.columns where table_name = '${table}';`;
  },


  connectionString: () => {
    return `psql -t postgres://postgres:hereliesafreeelf215@localhost/freshcoapp`;
  },

  getConstraints: () => {
    return `
      SELECT tc.constraint_name,
        tc.constraint_type,
        tc.table_name,
        kcu.column_name,
        tc.is_deferrable,
        tc.initially_deferred,
        rc.match_option AS match_type,

        rc.update_rule AS on_update,
        rc.delete_rule AS on_delete,
        ccu.table_name AS references_table,
        ccu.column_name AS references_field
      FROM information_schema.table_constraints tc

        LEFT JOIN information_schema.key_column_usage kcu
        ON tc.constraint_catalog = kcu.constraint_catalog
          AND tc.constraint_schema = kcu.constraint_schema
          AND tc.constraint_name = kcu.constraint_name

        LEFT JOIN information_schema.referential_constraints rc
        ON tc.constraint_catalog = rc.constraint_catalog
          AND tc.constraint_schema = rc.constraint_schema
          AND tc.constraint_name = rc.constraint_name

        LEFT JOIN information_schema.constraint_column_usage ccu
        ON rc.unique_constraint_catalog = ccu.constraint_catalog
          AND rc.unique_constraint_schema = ccu.constraint_schema
          AND rc.unique_constraint_name = ccu.constraint_name

      WHERE lower(tc.constraint_type) in ('foreign key', 'primary key');
    `;
  }
};